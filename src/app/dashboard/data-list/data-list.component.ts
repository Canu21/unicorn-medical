import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from '../../core/services/search.service';
import { StackData } from '../Stackdata';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent implements OnInit {

  @Input() issue: string;
  stackData: StackData[];

  constructor(private searchService: SearchService) { }

  ngOnInit() {
    this.searchService.search('TypeScript').subscribe(data => this.stackData = data['items']);
  }

}
