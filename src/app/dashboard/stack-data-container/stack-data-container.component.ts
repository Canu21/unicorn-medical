import { Component, Input } from '@angular/core';
import { StackData } from '../Stackdata';

@Component({
  selector: 'app-stack-data-container',
  templateUrl: './stack-data-container.component.html',
  styleUrls: ['./stack-data-container.component.scss']
})
export class StackDataContainerComponent {

  @Input() stackData: StackData;



}
