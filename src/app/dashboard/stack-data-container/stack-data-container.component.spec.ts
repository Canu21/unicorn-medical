import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackDataContainerComponent } from './stack-data-container.component';

describe('StackDataContainerComponent', () => {
  let component: StackDataContainerComponent;
  let fixture: ComponentFixture<StackDataContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackDataContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackDataContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
